**A simple static site generator in a single binary with rust**

- [Rust](https://www.rust-lang.org/)
- [Bootstrap](https://getbootstrap.com/)
- [Cloudflare Pages](https://pages.cloudflare.com/)
